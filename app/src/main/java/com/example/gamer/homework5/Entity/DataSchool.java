package com.example.gamer.homework5.Entity;

import android.net.Uri;

public class DataSchool {
    private String unName;
    private String unPhone;
    private String unWeb;
    private String unEmail;
    private String unAddress;
    private Uri pictsure;

    public DataSchool(){

    }

    public DataSchool(String unName, String unPhone, String unWeb, String unEmail, String unAddress, Uri pictsure) {
        this.unName = unName;
        this.unPhone = unPhone;
        this.unWeb = unWeb;
        this.unEmail = unEmail;
        this.unAddress = unAddress;
        this.pictsure = pictsure;
    }

    public String getUnName() {
        return unName;
    }

    public void setUnName(String unName) {
        this.unName = unName;
    }

    public String getUnPhone() {
        return unPhone;
    }

    public void setUnPhone(String unPhone) {
        this.unPhone = unPhone;
    }

    public String getUnWeb() {
        return unWeb;
    }

    public void setUnWeb(String unWeb) {
        this.unWeb = unWeb;
    }

    public String getUnEmail() {
        return unEmail;
    }

    public void setUnEmail(String unEmail) {
        this.unEmail = unEmail;
    }

    public String getUnAddress() {
        return unAddress;
    }

    public void setUnAddress(String unAddress) {
        this.unAddress = unAddress;
    }

    public Uri getPictsure() {
        return pictsure;
    }

    public void setPictsure(Uri pictsure) {
        this.pictsure = pictsure;
    }

    @Override
    public String toString() {
        return "DataSchool{" +
                "unName='" + unName + '\'' +
                ", unPhone='" + unPhone + '\'' +
                ", unWeb='" + unWeb + '\'' +
                ", unEmail='" + unEmail + '\'' +
                ", unAddress='" + unAddress + '\'' +
                ", pictsure=" + pictsure +
                '}';
    }
}
