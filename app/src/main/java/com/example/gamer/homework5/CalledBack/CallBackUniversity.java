package com.example.gamer.homework5.CalledBack;

import com.example.gamer.homework5.Entity.DataSchool;

public interface CallBackUniversity {

    void delete(DataSchool dataSchool, int postion);

    interface AddDialogUniversity{
        void getUniversity(DataSchool dataSchool);
    }
}
